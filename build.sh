#!/usr/bin/env bash
if [ $# -eq 0 ]; then
    port=26
elif [ $1 = "SUCHAI2" ] || [ $1 = "2" ]; then
    port=25
elif [ $1 = "SUCHAI3" ] || [ $1 = "3" ]; then
    port=26
elif [ $1 = "PLANTSAT" ] || [ $1 = "P" ]; then
    port=27
else
    port=$1
fi
printf "Selected TRX port: $port\n"
cmake -B build -DAPP=supermagp -DSCH_ARCH=RPI -DSCH_ST_MODE=SQLITE -DSCH_COMM_NODE=7 -DSCH_TRX_PORT_APP=$port -DSCH_STORAGE_FILE=\"/home/pi/smp\" && cmake --build build
