#!/usr/bin/env bash
wget http://www.airspayce.com/mikem/bcm2835/bcm2835-1.70.tar.gz
git clone -b feature/framework https://gitlab.com/spel-uchile/suchai-flight-software.git
git clone https://gitlab.com/matvidalv/FOD.git
git clone https://github.com/tmrh20/RF24.git
git clone https://gitlab.com/CGCSAMR/mag_plus.git
git clone -b framework https://gitlab.com/samuel-gutierrez/suchai-framework-stt.git
git clone https://gitlab.com/matvidalv/iot.git
