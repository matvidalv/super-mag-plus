# Super-mag-plus

The SUCHAI flight software is a program for CubeSats (satellites made of one or
multiple cubes of 10 cm). This program uses the command pattern. It is a modular
and extensible software and it can integrate different payloads or applications
inside one flight software. It can also interact with other microcontrollers and
processors running another instance of the flight sofware. This communication
between flight softwares can be done with either ZMQ or the CubeSat Space Protocol
library (libcsp).

The Super MAG+ is a PCB that contains and/or connects multiple sensors and components
such as a transceiver, magnetometers, an antenna array a camera, a Star Tracker, among
others. This board needs a software to interact with all these devices, transmit,
receive and save data, and communicate with other payloads using the libcsp. This
repository is intended to combine the applications for different sensors in one
application, using the latest version of the SUCHAI Flight Software.

Inside this repository you can find the necessary files that combines all the data
structures, system variables, compilation flags, and the main program for the application.
It also have a bash script that makes a pull of the other repositories and copy the
new files inside the repository of the SUCHAI flight software.

The final folder structure should be like in the following picture:

![](img/folder_structure.png)

## Getting started

First run the following command to download some files and repositories:

```shell
git clone https://gitlab.com/matvidalv/super-mag-plus.git
sh super-mag-plus/download_repos.sh 
```

Now, for the installation run the script init.sh inside the FOD repository and
install the dependencies for the Star Tracker:

```shell
cd FOD/
sudo sh init.sh
sudo apt-get install python3-serial python3-smbus python3-dev python3-pip i2c-tools libopenblas0 libopenjp2-7 -y
pip3 install astropy pillow
cd ../suchai-framework-stt/
sh stt_installer.sh
```

After installing all the dependencies, you can combine all the applications of
the SUCHAI flight software inside one application running the update_repositories.sh
bash script. You can also use it to update the other repositories and copy the
new updates into the SUCHAI flight software:

```shell
cd ../super-mag-plus
sh update_repositories.sh
```

Building for the Raspberry Pi is straight forward and very similar to any Linux.
Just connect to the RPi using SSH. Then, set proper build variables and build with
the following steps:

```shell
cd ../suchai-flight-software/
cmake -B build -DAPP=supermagp -DSCH_ARCH=RPI -DSCH_ST_MODE=SQLITE -DSCH_COMM_NODE=7 -DSCH_TRX_PORT_APP=26 && cmake --build build
./build/apps/supermagp/supermagp-app
```

## Running the SUCHAI Flight Software as a service

We simplified the process to enable the software to excecute each time the Raspberry turns on
using the following proceadure only once:

```shell
cd ../super-mag-plus
sudo cp suchai.service /lib/systemd/system/
sudo systemctl daemon-reload
sudo systemctl enable suchai.service
sudo systemctl start suchai.service
```

In order to show the service output, please use this command:

```shell
sudo journalctl -b -f -u suchai.service
```

If you want to stop the service to recompile the software and run it again you can use the
build.sh bash script. This script receives as an argument the number of the TRX port, which
is used to allow the ground station to know from which payload it is receiving the data. If
there are no arguments, the default port is 25.
There are some special numbers. The number 2 corresponds to the SUCHAI-2's Super-Mag+, the
number 3 is for SUCHAI-3's Mag+, and P is for the PlantSat's Mag-extreme.:

```shell
cd ../suchai-flight-software/
sudo sh build.sh 3
```

## Authors and acknowledgment

We want to acknowledge the following persons who have worked in the repositories related
with this one:

* Carlos González, Tomás Opazo, Camilo Rojas and Tamara Gutiérrez for their work on the SUCHAI flight
software.
* Cristóbal Garrido for the drivers and commands of the magnetometers.
* Matías Vidal Valladares for his implementation of the FOD hardware and software.
* Samuel Gutiérrez for developing a low-cost, open-source Star Tracker.

