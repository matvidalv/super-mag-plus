#!/usr/bin/env bash
printf "Updating the Super MAG+ software\n"
git pull
cp -r apps/supermagp ../suchai-flight-software/apps/
cp build.sh ../suchai-flight-software/build.sh

printf "Updating the Femto-satellite Orbital Deployer (FOD) software\n"
cd ../FOD/
git pull
cd ../suchai-flight-software/
cp ../FOD/check_frames.sh check_frames.sh
cp ../FOD/gen_cmds.py gen_cmds.py
cp ../FOD/apps/fodapp/src/system/cmdFOD.c apps/supermagp/src/system/cmdFOD.c
cp ../FOD/apps/fodapp/src/system/cmdRPI.c apps/supermagp/src/system/cmdRPI.c
cp ../FOD/apps/fodapp/src/system/cmdAOA.c apps/supermagp/src/system/cmdAOA.c
cp ../FOD/apps/fodapp/src/system/taskFOD.c apps/supermagp/src/system/taskFOD.c
cp apps/fodapp/src/system/bcm2835.c apps/supermagp/src/system/bcm2835.c
cp ../FOD/apps/fodapp/include/app/system/cmdFOD.h apps/supermagp/include/app/system/cmdFOD.h
cp ../FOD/apps/fodapp/include/app/system/cmdRPI.h apps/supermagp/include/app/system/cmdRPI.h
cp ../FOD/apps/fodapp/include/app/system/cmdAOA.h apps/supermagp/include/app/system/cmdAOA.h
cp ../FOD/apps/fodapp/include/app/system/taskFOD.h apps/supermagp/include/app/system/taskFOD.h

printf "Updating the software for the magnetometer\n"

cd ../mag_plus/
git pull
cp mag.txt ../mag.txt
cd ../suchai-flight-software/
cp ../mag_plus/suchai-flight-software/apps/myapp/src/system/cmdHello.c apps/supermagp/src/system/cmdHello.c
cp ../mag_plus/suchai-flight-software/apps/myapp/src/system/MCP9808.py apps/supermagp/src/system/MCP9808.py
cp ../mag_plus/suchai-flight-software/apps/myapp/src/system/Mag_init.py apps/supermagp/src/system/Mag_init.py
cp ../mag_plus/suchai-flight-software/apps/myapp/src/system/Mag_test.py apps/supermagp/src/system/Mag_test.py
cp ../mag_plus/suchai-flight-software/apps/myapp/src/system/RM3100.py apps/supermagp/src/system/RM3100.py
cp ../mag_plus/suchai-flight-software/apps/myapp/src/system/switch.py apps/supermagp/src/system/switch.py
cp ../mag_plus/suchai-flight-software/apps/myapp/include/app/system/cmdHello.h apps/supermagp/include/app/system/cmdHello.h

printf "Updating the Star Tracker (STT) software\n"

cd ../suchai-framework-stt/
git pull
cd ../suchai-flight-software/
cp ../suchai-framework-stt/apps/stt-suchai3/src/system/cmdSTT.c apps/supermagp/src/system/cmdSTT.c
cp ../suchai-framework-stt/apps/stt-suchai3/src/system/cmdAUX.c apps/supermagp/src/system/cmdAUX.c
cp ../suchai-framework-stt/apps/stt-suchai3/include/app/system/cmdSTT.h apps/supermagp/include/app/system/cmdSTT.h
cp ../suchai-framework-stt/apps/stt-suchai3/include/app/system/cmdAUX.h apps/supermagp/include/app/system/cmdAUX.h
cp -r ../suchai-framework-stt/stt_data .
cp -r ../suchai-framework-stt/tracking_data .
cp -r ../suchai-framework-stt/apps/stt-suchai3/stt_python_scripts apps/supermagp/

printf "Updating the IoT software\n"
cd ../iot/
git pull
cd ../suchai-flight-software/
cp ../iot/apps/IoT/src/system/cmdIOT.c apps/supermagp/src/system/cmdIOT.c
cp ../iot/apps/IoT/src/system/IoT.py apps/supermagp/src/system/IoT.py
cp ../iot/apps/IoT/src/system/IoT_RX.py apps/supermagp/src/system/IoT_RX.py
cp ../iot/apps/IoT/include/app/system/cmdIOT.h apps/supermagp/include/app/system/cmdIOT.h

printf "Updating the SUCHAI flight software\n"
git pull
