/*                                 SUCHAI
 *                      NANOSATELLITE FLIGHT SOFTWARE
 *
 *      Copyright 2024, Matias Vidal Valladares, matias.vidal.v@gmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "suchai/mainFS.h"
#include "suchai/taskInit.h"
#include "suchai/osThread.h"
#include "suchai/log_utils.h"
#include "app/system/taskHousekeeping.h"
#include "app/system/taskFOD.h"
#include "app/system/cmdFOD.h"
#include "app/system/cmdRPI.h"
#include "app/system/cmdSTT.h"
#include "app/system/cmdHello.h"
#include "app/system/cmdIOT.h"
#include "app/system/cmdAOA.h"
#include "app/system/cmdAUX.h"
#ifdef RPI
#include "csp_if_i2c_uart.h"
#endif

static char *tag = "app_main";

/**
 * App specific initialization routines
 * This function is called by taskInit
 *
 * @param params taskInit params
 */
void initAppHook(void *params)
{
    /** Include FOD commands */
    cmd_fod_init();

    /** Include RPI commands */
    cmd_rpi_init();

    /** Include STT commands */
    cmd_stt_init();

    /** Include magnetometer commands */
    cmd_hello_init();

    /** Include IoT commands */
    cmd_iot_init();

    /** Include AOA commands */
    cmd_aoa_init();

    /** Include STT auxiliary commands */
    cmd_aux_init();

    /** Initialize custom CSP interfaces */
#if defined(X86)
    csp_add_zmq_iface(SCH_COMM_NODE);
#elif defined(RPI)
    csp_i2c_uart_init(SCH_COMM_NODE, 0, 19200);
    csp_rtable_set(8, 2, &csp_if_i2c_uart, 5);
    csp_route_set(CSP_DEFAULT_ROUTE, &csp_if_i2c_uart, CSP_NODE_MAC);
#endif

    /** Init app tasks */
    int t_ok = osCreateTask(taskHousekeeping, "housekeeping", 1024, NULL, 2, NULL);
    if(t_ok != 0) LOGE("supermagp-app", "Task housekeeping not created!");
    int tfod_ok = osCreateTask(taskFOD, "FOD", 1024, NULL, 2, NULL);
    if(tfod_ok != 0) LOGE("supermagp-app", "Task FOD not created!");
}

int main(void)
{
    /** Call framework main, shouldn't return */
    suchai_main();
}
